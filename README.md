# CraftBot

Simple discord.py bot for sending commands to the Crafty Controller
API.

See [their repo](https://gitlab.com/crafty-controller/crafty-web) for more info
about Crafty

## Commands

| Command          | Arguments              | Description                                                                       |
|------------------|------------------------|-----------------------------------------------------------------------------------|
| `host_stats`     |                        | Displays CPU/Memory/Disk information about the Crafty host                        |
| `server_backup`  | `server_id`            | Triggers a backup of the server (DOES NOT WORK YET)                               |
| `server_command` | `server_id`, `command` | Sends `command` to the server console and prints the logs after                   |
| `server_list`    |                        | Lists all servers by ID and shows their running status                            |
| `server_logs`    | `server_id`            | Prints the most recent log lines from the server with ID `server_id`              |
| `server_query`   | `server_id`            | Prints a brief status about the server with ID `server_id`                        |
| `server_restart` | `server_id`            | Retarts server with ID `server_id`                                                |
| `server_start`   | `server_id`            | Starts server with ID `server_id`                                                 |
| `server_stats`   | `server_id`            | Prints statistics and information about the process of server with ID `server_id` |
| `server_stop`    | `server_id`            | Stops server with ID `server_id`                                                  |


## Installation

I'd recommend setting up a virtualenv for this to run in so you don't have
to install to the system prefix, but it's technically optional

Prerequisites:

| Requirement     | Version  | Explanation                                        |
|-----------------|----------|----------------------------------------------------|
| `discord.py`    | >= 1.2.5 | for connecting to discord                          |
| `jsmin`         |          | for supporting comments in json                    |
| `tabulate`      |          | for creating fixed-width text tables in embeds     |
| `crafty-client` | >= 2.0.0 | the star of the show :) / for connecting to Crafty |

1. Make the directory/user you want to run CraftBot in (I use one near where I
   run Crafty `/var/opt/minecraft/craftbot`)
   - `mkdir /var/opt/minecraft/craftbot`
2. Go there
   - `cd /var/opt/minecraft/craftbot`
3. Get the code
   - `git clone https://gitlab.com/computergeek125/CraftBot.git`
3. (optional, but recommended) Set up virtualenv
   - `python3 -m venv craftbot-venv`
   - `source ./craftbot-venv/bin/activate`
4. Install prerequisites
   - `python3 -m pip install -U -r requirements.txt`

## Configuration

To copy an example config, `cp config.default.json config.json` from the Git repo

There are four parameters in `config.json`:

 - `discord_token`: API key for connecting to Discord. See
   [Discord's developer page](https://discord.com/developers) for more info.
 - `crafty_url`: URL that the crafty client will connect to.  Typically, 
   `https://localhost:8000` is a sane default for most systems where CraftBot
   is running on the same server as Crafty, but you may have changed the port
   number, or may be running CraftBot on a different server
 - `crafty_token`: API key for connecting to Crafty.  This can be seen and re-
   generated on Crafty's Config page
 - `channel_binds`: The integer ID of channels where the bot can accept commands.
   In the Discord app, enable Developer Mode and right-click a channel, then "Copy
   ID". `channel_binds` is a list, but will run with a list length of 1 for only
   one channel

## Running

The bot's first and only parameter is the filename for the config file, defaulting to `config.json`.

Option 1: default config file location:

 - `cd /var/opt/minecraft/craftbot/CraftBot`
 - `python3 craftbot.py`

Option 2: setting config file location:

 - `cd /var/opt/minecraft/craftbot/CraftBot`
 - `python3 craftbot.py my_config_file.json`

## systemd

Optionally, you can launch this 'bot with the machine startup using the systemd unit provided in the `systemd/` folder in this repo

### Installation
1. Copy `<repository>/systemd/craftbot.service` to `/etc/systemd/system`
2. Copy `<repository>/systemd/craftbot` to `/etc/default/` (there WILL be a directory here, if there is not, open an issue, I probably have a typo or you have a distro I didn't test)
3. (Optional) Edit `/etc/default/craftbot` and modify the variable `CRAFTBOT_ARGS` to add any run-time arguments you wish to use with the bot
4. Change the `User=` and `Group=` value to a service account that you wish to run the bot as (this can be the same user that Crafty uses, but does not have to be)
5. Change ownership of the directory you cloned to the user and group of the previous step.
    - i.e., if you are using the `crafty` user and your base directory is `/var/opt/minecraft/craftbot`, the command would be `sudo chown -R crafty. /var/opt/minecraft/craftbot`
5. Change `WorkingDirectory=` and `ExecStart=` to match the directory you cloned the repository to and the location of the `python` binary within the virtual environment you set up during the Installation step
6. If this will be running on the same machine that runs Crafty, change the name of `crafty4.service` to match the name of the systemd unit you use to automatically start crafty
    - If this will be run on a different machine, remove `crafty4.service` from the `After=` directive
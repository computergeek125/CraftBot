import argparse
import json
import pprint
import socket
import sys
import time
import traceback
import tabulate
from crafty_client import Crafty4
from crafty_client.static import exceptions as crafty_exc
import discord
from discord.ext import commands


try:
    import jsmin

    JSMIN_AVAIL = True
except ImportError:
    JSMIN_AVAIL = False
    sys.stderr.write(
        "WARN: Could not load jsmin. Comments in the config file will not be supported.\n"
    )

parser = argparse.ArgumentParser(
    prog="CraftBot",
    description="Simple bot to interface between discord channel commands and the Crafty Controller server panel",
)
parser.add_argument(
    "-c",
    "--config-file",
    help="json file to read configuration from - see config.default.json",
    type=str,
    default="config.json",
)
parser.add_argument(
    "-p", "--command-prefix", help="command prefix to listen for in discord", type=str
)
parser.add_argument(
    "--client-debug",
    help="enables extra debug information to be printed by the Crafty Client **WARNING: DO NOT USE IN PRODUCTION**",
    action="store_true",
    default=False,
)
parser.add_argument(
    "--nomin",
    help="temporarily disable jsmin to debug config file glitches",
    action="store_true",
    default=False,
)
parser.add_argument(
    "munch", help=argparse.SUPPRESS, nargs="?", type=str, default=""
)  # suppresses failure when the last option is an empty string
args = parser.parse_args()

config_base = {
    "command_prefix": "$",
    "command_log_delay": 3,
    "discord_embed_width": 61,
    "server_logs_default_len": 10,
    "server_list_fields": ["running", "crashed", "auto_start"],
    "server_stat_fields": [
        "desc",
        "version",
        "running",
        "started",
        "crashed",
        "_online_max",
        "cpu",
        "mem",
        "world_size",
    ],
}

config_args = {
    "client_debug": args.client_debug,
    "command_prefix": args.command_prefix,
}


def config_merge(*configs):
    final_config = {}
    for c in configs:
        for k in c:
            if c[k] is not None:
                final_config[k] = c[k]
    return final_config


with open(args.config_file, encoding="utf-8") as config_file:
    if JSMIN_AVAIL or not args.nomin:
        config_data = json.loads(jsmin.jsmin(config_file.read()))
    else:
        config_data = json.load(config_file)

config = config_merge(config_base, config_data, config_args)

cweb = Crafty4(
    config["crafty_url"],
    config["crafty_token"],
    verify_ssl=config["verify_ssl"],
    debug=config["client_debug"],
)

intents = discord.Intents.default()
intents.message_content = True
bot = commands.Bot(command_prefix=config["command_prefix"], intents=intents)


def make_embed(title):
    e = discord.Embed(title=f"{title}", color=0x00FF00)
    e.set_author(name=f"{socket.gethostname()}")
    return e


async def crafty_control(ctx, action, id_str, *act_args):
    embed = make_embed("Server control")
    embed_line_width = config["discord_embed_width"] * 2
    id_str = str(id_str)
    status = False
    try:
        stats = cweb.get_server_stats(id_str)
        metadata = stats["server_id"]
    except crafty_exc.AccessDenied:
        embed.color = 0xFF00FF
        embed.add_field(
            name=f"{action} id={id_str}",
            value=f"ERROR: The bot account on Crafty Controller was denied access to server with ID {id_str}, or a server with that ID could not be found",
            inline=False,
        )
        return embed
    try:
        if action == "start":
            status = cweb.start_server(id_str)
        elif action == "stop":
            status = cweb.stop_server(id_str)
        elif action == "restart":
            status = cweb.restart_server(id_str)
        elif action == "query":
            status = True
        elif action == "command":
            cmd_res = cweb.run_command(id_str, *act_args)
            intermediate_embed = make_embed("Server control")
            intermediate_embed.color = 0xFFFF00
            intermediate_embed.add_field(
                name=f'{action} {metadata["server_name"]}',
                value=f"Running command `{act_args}`, sending logs in {config['command_log_delay']} seconds",
                inline=False,
            )
            if config["client_debug"]:
                intermediate_embed.add_field(
                    name="Command result", value=f"{pprint.pformat(cmd_res)}"
                )
            intermediate_message = await ctx.send(embed=intermediate_embed)
            time.sleep(config["command_log_delay"])
            logs = cweb.get_server_logs(id_str)
            await intermediate_message.delete()
            status = True
        elif action == "logs":
            logs = cweb.get_server_logs(id_str)
            status = True
        elif action == "backup":
            status = cweb.backup_server(id_str)
        else:
            embed.color = 0xFF00FF
            embed.add_field(
                name="command error",
                value=f"somehow, SOMEHOW, you got the action {action} into an internal function.  You should be proud of yourself and report this to the developer",
                inline=False,
            )
    except crafty_exc.ServerNotFound:
        embed.color = 0xFF0000
        embed.add_field(
            name=f'{action} {metadata["server_name"]}',
            value=f"ERROR: Server with ID {id_str} not found!",
            inline=False,
        )
    except crafty_exc.ServerNotRunning:
        embed.color = 0xFF0000
        embed.add_field(
            name=f'{action} {metadata["server_name"]}',
            value="ERROR: Server is not running",
            inline=False,
        )
    except crafty_exc.ServerAlreadyRunning:
        embed.color = 0xFF0000
        embed.add_field(
            name=f'{action} {metadata["server_name"]}',
            value="ERROR: Server is already running",
            inline=False,
        )
    except crafty_exc.AccessDenied:
        embed.color = 0xFF00FF
        embed.add_field(
            name=f'{action} {metadata["server_name"]}',
            value="ERROR: The bot account on Crafty Controller was denied access to this command",
            inline=False,
        )
    except crafty_exc.MissingParameters as e:
        embed.color = 0xFF00FF
        embed.add_field(
            name=f'{action} {metadata["server_name"]}',
            value=f"ERROR: How did you even do this??? Missing parameters: {e}",
            inline=False,
        )
    except crafty_exc.NotAllowed as e:
        embed.color = 0xFF00FF
        embed.add_field(
            name=f'{action} {metadata["server_name"]}',
            value=f"ERROR: I am legit not sure what this even means. Not allowed: {e}",
            inline=False,
        )
    if status:
        if action == "query":
            embed.add_field(
                name=f'{action} {metadata["server_name"]}',
                value=f'Running:\t{stats["running"]}\nCrashed:\t{stats["crashed"]}\nAutostart:\t{metadata["auto_start"]}',
                inline=False,
            )
            if not stats["running"]:
                embed.color = 0xFF0000
        elif action == "logs" or action == "command":
            try:
                if len(act_args) <= 0 or action == "command":
                    line_selection = logs[-config["server_logs_default_len"] :]
                elif len(act_args) >= 2:
                    lines = int(act_args[0])
                    offset = int(act_args[1])
                    line_selection = logs[-lines - offset : -offset]
                elif len(act_args) == 1:
                    lines = int(act_args[0])
                    line_selection = logs[-lines:]
                else:
                    embed.color = 0xFF0000
                    embed.add_field(
                        name=f'{action} {metadata["server_name"]}',
                        value=f"ERROR: Something went terribly wrong while parsing the arguments {act_args}",
                        inline=False,
                    )
                    return embed
            except ValueError as e:
                embed.color = 0xFF0000
                embed.add_field(
                    name=f'{action} {metadata["server_name"]}',
                    value=f"ERROR: Could not parse value when executing command: {e}",
                    inline=False,
                )
                return embed
            line_trunc = False
            for l in range(len(line_selection)):
                if len(line_selection[l]) > embed_line_width:
                    truncated_line = line_selection[l][0 : embed_line_width - 2]
                    truncated_line += "\u2026"
                    line_trunc = True
                    line_selection[l] = truncated_line
            if len(line_selection) == 0:
                embed.color = 0xFF0000
                embed_text = "ERROR: no logs could be found - perhaps your server has not run yet"
                if len(act_args) >= 2:
                    embed_text += " or your requested offset was too high"
            else:
                trimmed_ls = 0
                joined_ls = "\n".join(line_selection)
                while len(joined_ls) > (1024 - 8):
                    line_selection.pop(0)
                    joined_ls = "\n".join(line_selection)
                    trimmed_ls += 1
                if trimmed_ls > 0:
                    embed.add_field(
                        name="Warning:",
                        value=f"**{trimmed_ls}** lines were omitted from the beginning of this response since it exceeded discord limitations",
                        inline=False,
                    )
                if line_trunc:
                    embed.add_field(
                        name="Warning:",
                        value="Some long lines were truncated with `\u2026`.  See server panel for full log details.\n(this message may be visble if a line was too long prior to omission)",
                        inline=False,
                    )
                embed_text = f"```\n{joined_ls}\n```"

            embed.add_field(
                name=f'{metadata["server_name"]} logs', value=embed_text, inline=False
            )
        else:
            embed.add_field(
                name=f'{action} {metadata["server_name"]}',
                value="Action successful!",
                inline=False,
            )
    return embed


def verify_channel(ctx):
    return ctx.message.channel.id in config["channel_binds"]


@bot.event
async def on_ready():
    print(f"Logged in as {bot.user} and ready to start crafting!")


@bot.event
async def on_command_error(ctx, error):
    if not verify_channel(ctx):
        return
    # https://stackoverflow.com/a/66831004
    embed = make_embed("ERROR")
    embed.color = 0xFF0000
    embed.add_field(name="Message:", value=ctx.message.content, inline=False)
    embed.add_field(
        name="Command:",
        value=f"{ctx.command} args={ctx.args[1:]} kwargs={ctx.kwargs}",
        inline=False,
    )
    embed.add_field(name="Error:", value=f"{error}", inline=False)
    dpy_traceback = "\n".join(traceback.format_tb(error.__traceback__))
    if len(dpy_traceback) > (1024 - 9):
        dpy_traceback = "\n".join(dpy_traceback[:1022] + "\u2026")
    embed.add_field(
        name="Discord traceback:", value=f"```py\n{dpy_traceback}```", inline=False
    )
    if hasattr(error, "original"):
        py_traceback = "\n".join(traceback.format_tb(error.original.__traceback__))
        if len(dpy_traceback) > (1024 - 9):
            py_traceback = py_traceback[:1022] + "\u2026"
        embed.add_field(
            name="Python traceback:", value=f"```py\n{py_traceback}```", inline=False
        )
    await ctx.send(embed=embed)


@bot.command()
@commands.check(verify_channel)
async def server_list(ctx):
    sv_list = cweb.list_mc_servers()
    table = []
    for s in sv_list:
        stats = cweb.get_server_stats(s["server_id"])
        table_row = {}
        table_row["id"] = s["server_id"]
        table_row["name"] = s["server_name"]
        for k in config["server_list_fields"]:
            try:
                table_row[k] = stats[k]
            except KeyError:
                try:
                    table_row[k] = s[k]
                except KeyError:
                    sys.stderr.write(
                        f"ERROR: Could not locate a value for key {k} when listing server {s['server_id']}"
                    )
        table.append(table_row)
    embed = make_embed("Server list")
    tabulated = tabulate.tabulate(table, headers="keys")
    embed.add_field(
        name="Servers:",
        value=f"```{tabulated}```",
    )
    await ctx.send(embed=embed)


@bot.command()
@commands.check(verify_channel)
async def server_stats(ctx, *cmd_args):
    servers_to_list = []
    if len(cmd_args) <= 0:
        for s in cweb.list_mc_servers():
            servers_to_list.append(s["server_id"])
    else:
        for s in cmd_args:
            servers_to_list.append(s)
    for s in servers_to_list:
        stats = cweb.get_server_stats(s)
        table = []
        for k in config["server_stat_fields"]:
            if k == "_online_max":
                table.append(["players", f"{stats['online']}/{stats['max']}"])
            else:
                table.append([k, str(stats[k])])
        embed = make_embed("Server stats")
        tabulated = tabulate.tabulate(table)
        embed.add_field(
            name=stats["server_id"]["server_name"],
            inline=False,
            value=f"```{tabulated}```",
        )
        await ctx.send(embed=embed)


@bot.command()
@commands.check(verify_channel)
async def host_stats(ctx):
    stats = cweb.get_host_stats()
    embed = make_embed("Host stats:")
    embed.add_field(
        name="CPU:",
        inline=False,
        value=f'{stats["cpu_usage"]}% of {stats["cpu_cores"]} cores @ {stats["cpu_cur_freq"] / 1000.0} GHz (max {stats["cpu_max_freq"] / 1000.0} GHz)',
    )
    embed.add_field(
        name="Memory:",
        inline=False,
        value=f'{stats["mem_percent"]}%: {stats["mem_usage"]} / {stats["mem_total"]}',
    )
    disks = []
    mounts_ignore = ("/snap/",)
    try:
        disk_stats = json.loads(stats["disk_json"])
    except json.decoder.JSONDecodeError:
        disk_stats = json.loads(stats["disk_json"].replace("'", '"'))
    for d in disk_stats:
        if d["mount"].startswith(mounts_ignore):
            continue
        disks.append(
            [
                d["mount"],
                d["fs"],
                f'{d["percent_used"]}%',
                d["used"],
                "/",
                d["total"],
            ]
        )
    tabulated = tabulate.tabulate(
        sorted(disks, key=lambda x: x[0]),
        headers=["mount", "fstype", "%", "used", "/", "total"],
        colalign=["left", "left", "right", "right", "center", "right"],
    )
    embed.add_field(
        name="Disk:",
        inline=False,
        value=f"```{tabulated}```",
    )
    embed.set_footer(text=f'ID:{stats["id"]}')
    await ctx.send(embed=embed)


@bot.command()
@commands.check(verify_channel)
async def server_query(ctx, arg):
    e = await crafty_control(ctx, "query", arg)
    await ctx.send(embed=e)


@bot.command()
@commands.check(verify_channel)
async def server_start(ctx, arg):
    e = await crafty_control(ctx, "start", arg)
    await ctx.send(embed=e)


@bot.command()
@commands.check(verify_channel)
async def server_stop(ctx, arg):
    e = await crafty_control(ctx, "stop", arg)
    await ctx.send(embed=e)


@bot.command()
@commands.check(verify_channel)
async def server_restart(ctx, arg):
    e = await crafty_control(ctx, "restart", arg)
    await ctx.send(embed=e)


@bot.command()
@commands.check(verify_channel)
async def server_command(ctx, sv_id, command):
    e = await crafty_control(ctx, "command", sv_id, command)
    await ctx.send(embed=e)


@bot.command()
@commands.check(verify_channel)
async def server_logs(ctx, sv_id, *arg):
    e = await crafty_control(ctx, "logs", sv_id, *arg)
    await ctx.send(embed=e)


@bot.command()
@commands.check(verify_channel)
async def server_backup(ctx, arg):
    e = await crafty_control(ctx, "backup", arg)
    await ctx.send(embed=e)


bot.run(config["discord_token"])
